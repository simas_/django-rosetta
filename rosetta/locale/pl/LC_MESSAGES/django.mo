��    "      ,      <      <  H   =  '   �     �     �     �     �     �     �  7   �     &  
   ,     7     :     ?     R     [     q     z     �     �  �   �     4  !   =     _     }     �  �   �  )   $  
   N     Y     i  t   {     �  U  �  N   N  -   �     �  	   �  
   �     �     �       A        S     \     k  	   p     z  
   �     �     �     �     �     �  �   �     �	  !   �	      �	     �	     �	  �   
  +   �
     �
     �
       p         �   %(hits)s/%(message_number)s message %(hits)s/%(message_number)s messages %(more_count)s more %(more_count)s more All Application Display: Displaying: Download this catalog File File is read-only: download the file when done editing! Fuzzy Fuzzy only Go Home Language selection Messages Nothing to translate! Obsolete Occurrences(s) Original Pick another file Please refer to <a href="%(i18n_doc_link)s">Django's I18N documentation</a> for a guide on how to set up internationalization for your project. Progress Progress: %(percent_translated)s% Save and translate next block Search Skip to page: Some items in your last translation block couldn't be saved: this usually happens when the catalog file changes on disk after you last loaded it. Translate into %(rosetta_i18n_lang_name)s Translated Translated only Untranslated only You haven't specified any languages in your settings file, or haven't yet generated a batch of translation catalogs. suggest Project-Id-Version: Rosetta
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-07-04 13:42+0200
PO-Revision-Date: 2013-11-15 14:52+0100
Last-Translator:   <awrowhh@jadamspam.pl>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Translated-Using: django-rosetta 0.7.3
 %(hits)s/%(message_number)s komunikat %(hits)s/%(message_number)s komunikatów %(more_count)s więcej %(more_count)s więcej Wszystko Aplikacja Wyświetl: Wyświetlanie: Pobierz ten katalog Plik Plik jest tylko do odczytu: pobierz go kiedy skończysz edytować Niepewne Tylko niepewne Idź Początek Wybór języka Komunikaty Nie ma nic do przetłumaczenia! Przestarzałe Wystąpienie(a) Oryginalny tekst Wybierz inny plik Sprawdź pomoc dotyczącą <a href="%(i18n_doc_link)s">internacjonalizacji Django I18N </a>, aby znaleźć dokumentację na temat internacjonalizacji twojego projektu. Postęp Postęp: %(percent_translated)s % Zapisz i tłumacz następny blok Szukaj Przejdź do strony: Niektóre tłumaczenia z ostatniego bloku nie mogły zostać zapisane: zdarza się to zazwyczaj, gdy zawartość katalogu na dysku zmieni się po ostatnim przeładowaniu strony. Przetłumacz na %(rosetta_i18n_lang_name)s  Przetłumaczone Tylko przetłumaczone Tylko nieprzetłumaczone Nie podałeś żadnych języków w plikach ustawień, lub nie wygenerowałeś jeszcze katalogów do tłumaczeń. sugeruj 